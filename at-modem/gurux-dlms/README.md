# Gurux DLMS

## Description

The Gurux DLMS demo allows a DLMS client located in the cloud to request values to a DLMS server, this setup works on the Request-Response model.

This example provides a device application (DLMS server) and a cloud application (DLMS client). The cloud application periodically starts sending a series of DLMS requests in order to get information from the device application which simulates a DLMS meter.

The example demonstrates how the SCHC SDK through the use of AT commands can be used to exchange IPv6/UDP/DLMS frames between the device and the cloud application.

### SCHC compression/fragmentation

In this example we compress all the data from the IPv6, UDP, and DLMS wrapper headers so that only the DLMS payload is sent over the air. This represents 56 (40 + 8 + 8) bytes that are not sent per message. Compression rule ID 30 is used.

In case fragmentation is needed, AckOnError mode is used by the SDK. Uplink packets are fragmented using rule ID 20, while downlink packets are processed with rule ID 21.

### DTLS encryption

This application can use a SCHC library that provides security to the transferred data by making use of the DTLS protocol: compressed SCHC packets are ciphered using this protocol before being sent over the air.

If the appropriate firmware is loaded onto the board, then DTLS encryption shall be used by the SDK. In order to properly use this feature then the `--psk-id` and `--psk` parameters must be given with the same values provided to the SCHC Gateway.

### Synchronization

If supported by the SCHC Gateway, template parameter synchronization may be enabled for the TUN - AT Modem controller device application by providing the `--sync` flag.

## Class-A specification

To run this example using a class-A device, `--dev-class A` parameter must be set. Besides, in order to enable polling message during a fragmentation session, the parameter `--polling-conf 1` must be given. Polling messages interval may be set with `--polling-interval X`, where `X` is the periodicity of the polling message (default value is 60 seconds if not set).

## Requirements

* Ubuntu 20.04 LTS (or later)
* Python3
* pip3
* Java
* Maven

## Supported targets

* [STM32L476RG-Nucleo](https://os.mbed.com/platforms/ST-Nucleo-L476RG) board with either of the following extension shields:
    * [SX1276](https://www.semtech.com/products/wireless-rf/lora-connect/sx1276mb1mas)
    * [SX1272](https://www.semtech.com/products/wireless-rf/lora-connect/sx1272mb2das)
    * [SX1261](https://www.semtech.com/products/wireless-rf/lora-core/sx1261mb2bas)
* [B-L072Z-LRWAN1](https://www.st.com/en/evaluation-tools/b-l072z-lrwan1.html) board
* [Semtech LR1110DVK1TBKS](https://www.semtech.com/products/wireless-rf/lora-edge/lr1110dvk1tbks) development kit

## Run the example

Please read the [online documentation](https://lab-schc.gitlab.io/docs/4-Manual/d1-apps/2-atm-dlms/).

> __Note:__ In this Demo example, in order to avoid routing issues, the combination "DLMS server and TUN controller" must be running on a **different machine** than the combination "VPN agent and HES".

### Device

1. Flash the AT Modem firmware onto the board.

2. Set up the tunnel controller interface and routes to the cloud application address using the `tun_script.sh` script.

```
sudo ./tun_script.sh <interface_name> <dev_ipv6_addr> <cloud_ipv6_prefix>/64
```

3. Build and run the DLMS server example.

```
mvn package
java -jar target/gurux.dlms.server-1.jar
```

4. Connect the board and run the tunnel python application.

```
python3 -m tun_app --template rules.bin --dev-eui 11:11:11:11:11:11:11:11 --app-eui 00:00:00:00:00:00:00:00 --app-key 11:11:11:11:11:11:11:11:11:11:11:11:11:11:11:11 --ipv6-dev-prefix 5454:0000:0000:0000 --ipv6-dev-iid 0000:0000:0000:0003 --ipv6-app-prefix abcd:0000:0000:0000 --ipv6-app-iid 0000:0000:0000:0001 --udp-app-port 22224 --udp-dev-port 4063 --tun-name tun-ctrl
```

### Cloud

1. Patch the client and install the requirements.

```
./setup.sh
```

2. If required by the SCHC Gateway, run the VPN tunneling agent and add the IPv6 address for the cloud application.

```
sudo ip -6 address add abcd::1 dev tun-agent
```

3. Run the DLMS client.

```
env DLMS_SERVER_ADDRESS=<dev_ipv6_addr> DLMS_SERVER_PORT=4063 DLMS_CLIENT_ADDRESS=<app_ipv6_addr> DLMS_CLIENT_PORT=<app_port> DLMS_READ_TIMEOUT=<timeout> DLMS_SESSION_INTERVAL=<interval> DLMS_REQUEST_DELAY=<delay> python3 main.py
```
