# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

import logging
import pathlib
import time

from .module import Module
from collections import namedtuple

Command = namedtuple('Command', 'request expected_responses')
ping_cmd = Command('AT+SCHC=VERSION', ['OK'])
set_deveui_cmd = Command('AT+DEUI={}', ['OK'])
set_appeui_cmd = Command('AT+APPEUI={}', ['OK'])
set_appkey_cmd = Command('AT+APPKEY={}', ['OK'])
join_cmd = Command('AT+JOIN={}', ['+JOINED'])
dtls_join_cmd = Command('AT+JOINDTLS={}', ['+HANDSHAKEOK'])
sync_cmd = Command('AT+SCHC=SYNC,{},{}', ['+SYNCOK'])
set_template_cmd = Command('AT+SCHC=TPL,SET,{}', ['OK'])
set_template_param_cmd = Command(
    'AT+SCHC=TPLPARAMS,SET,{},{}', ['OK', 'AT_ERROR,1,1'])
get_template_param_cmd = Command(
    'AT+SCHC=TPLPARAMS,GET,{0}', ['OK', 'AT_ERROR,1,1'])
set_dr_cmd = Command('AT+DR={}', ['OK'])
sendb_data_cmd = Command('AT+SCHC=SENDB,{}', ['+SENDOK', '+SENDFAIL'])
receive_data_cmd = Command(None, ['+RECVOK', '+RECVFAIL'])
set_traces_cmd = Command('AT+TRACE={}', ['OK'])
conf_dtls_cmd = Command('AT+SCHC=DTLSCONF,SET,{},{},{},{}', ['OK'])
disable_echo_cmd = Command('ATE={}', ['OK'])
set_polling_conf_cmd = Command('AT+SCHC=CONF_POLL,{},1', ['OK'])
trigger_polling_cmd = Command('AT+SCHC=TRG_POLL', ['OK'])

TIMEOUT_DEFAULT_COMMAND = 1
TIMEOUT_JOIN_COMMAND = 10
TIMEOUT_DTLS_JOIN_COMMAND = 30


class Communication_Parameters:
    def __init__(self, dev_prefix, dev_iid, app_prefix, app_iid, dev_port, app_port):
        self.dev_ip = self._convert_ip_format(dev_prefix, dev_iid)
        self.app_ip = self._convert_ip_format(app_prefix, app_iid)
        self.dev_port = str(int(dev_port, 16))
        self.app_port = str(int(app_port, 16))

    def _convert_ip_format(self, prefix, iid):
        ip = prefix + iid
        return ':'.join(ip[i:i+4] for i in range(0, len(ip), 4))

    def get_dev_ip_addr(self):
        return self.dev_ip

    def get_app_ip_addr(self):
        return self.app_ip

    def get_dev_udp_port(self):
        return self.dev_port

    def get_app_udp_port(self):
        return self.app_port


class ATModem(Module):
    def __init__(self, device):
        super().__init__(device)
        self.joined = False
        self.template_parameters_set = False
        self.timeout = 0

    def configure_modem(self, args):
        self._dev_class = args.dev_class
        self._dev_eui = args.dev_eui
        self._app_eui = args.app_eui
        self._app_key = args.app_key
        self._dr = args.dr
        self._sync_retrans_dur = args.sync_retrans_dur
        self._sync_retrans_cnt = args.sync_retrans_cnt
        self._template = args.template
        self._psk_id = args.psk_id
        self._psk = args.psk
        self._dtls_timeout = args.dtls_timeout
        self._dtls_retrans_cnt = args.dtls_retrans_cnt
        self._polling_conf = args.polling_conf
        self._polling_interval = args.polling_interval
        self.dtls = False

        self.ping()
        self.disable_echo()
        if args.traces is True:
            self.set_traces(1)
        self.set_dev_eui(self._dev_eui)
        self.set_app_eui(self._app_eui)
        self.set_app_key(self._app_key)
        if self._psk_id is not None and self._psk is not None:
            self.conf_dtls(self._psk_id, self._psk, self._dtls_timeout,
                           self._dtls_retrans_cnt)
        self.join(self._dev_class)
        self.set_dr(self._dr)
        self.set_template(self._template)

        if args.ipv6_dev_prefix is not None:
            dev_prefix = args.ipv6_dev_prefix.replace(":", "")
            self._ipv6_dev_prefix = dev_prefix + ('0' * (16 - len(dev_prefix)))
            self.set_communication_param(0, self._ipv6_dev_prefix)

        if args.ipv6_dev_iid is not None:
            dev_iid = args.ipv6_dev_iid.replace(":", "")
            self._ipv6_dev_iid = ('0' * (16 - len(dev_iid))) + dev_iid
            self.set_communication_param(1, self._ipv6_dev_iid)

        if args.ipv6_app_prefix is not None:
            app_prefix = args.ipv6_app_prefix.replace(":", "")
            self._ipv6_app_prefix = app_prefix + ('0' * (16 - len(app_prefix)))
            self.set_communication_param(2, self._ipv6_app_prefix)

        if args.ipv6_app_iid is not None:
            app_iid = args.ipv6_app_iid.replace(":", "")
            self._ipv6_app_iid = ('0' * (16 - len(app_iid))) + app_iid
            self.set_communication_param(3, self._ipv6_app_iid)

        if args.udp_dev_port is not None:
            self._udp_dev_port = f'{int(args.udp_dev_port):04x}'
            self.set_communication_param(4, self._udp_dev_port)

        if args.udp_app_port is not None:
            self._udp_app_port = f'{int(args.udp_app_port):04x}'
            self.set_communication_param(5, self._udp_app_port)

        # Enable polling during downlink fragmentation session for class-A device
        if (self._dev_class == 'A'):
            if (self._polling_conf):
                # Enable polling and suspend uplinks during fragmentation session
                self.set_polling_conf(1)
            else:
                # default
                self.set_polling_conf(0)

        if (args.sync):
            self.sync(self._sync_retrans_dur, self._sync_retrans_cnt)
        self._prepare_communication_parameters()

        logging.info("device IP address: %s",  self.dev_ip)
        logging.info(
            "Sending dummy uplink in order to activate LNS side functionalities")
        self.sendb_data(b'\0')

    def _prepare_communication_parameters(self):
        dev_prefix = self.get_communication_param(0)
        dev_iid = self.get_communication_param(1)
        app_prefix = self.get_communication_param(2)
        app_iid = self.get_communication_param(3)
        dev_port = self.get_communication_param(4)
        app_port = self.get_communication_param(5)
        comm_param = Communication_Parameters(dev_prefix, dev_iid, app_prefix,
                                              app_iid, dev_port, app_port)
        self.dev_ip = comm_param.get_dev_ip_addr()
        self.app_ip = comm_param.get_app_ip_addr()
        self.dev_udp_port = comm_param.get_dev_udp_port()
        self.app_udp_port = comm_param.get_app_udp_port()

    def ping(self):
        '''Checks whether the module is responding'''
        self.send(ping_cmd.request)
        self.receive(keywords=ping_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND,
                     must_quit=True)

    def set_dev_eui(self, deveui):
        '''
        Sets device EUI

        Parameters:
            dev_eui (string): Device EUI
        '''
        self.send(set_deveui_cmd.request, deveui)
        self.receive(keywords=set_deveui_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def set_app_eui(self, appeui):
        '''
        Sets application EUI

        Parameters:
            app_eui (string): Application EUI
        '''
        self.send(set_appeui_cmd.request, appeui)
        self.receive(keywords=set_appeui_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def set_app_key(self, appkey):
        '''
        Sets application key

        Parameters:
            app_key (string): Application key
        '''
        self.send(set_appkey_cmd.request, appkey)
        self.receive(keywords=set_appkey_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def join(self, dev_class):
        '''
        Joins to the network server and configures device class

        Parameters:
            dev_class (string): Class
        '''
        self.dev_class = dev_class
        if self.dtls is True:
            self.send(dtls_join_cmd.request, dev_class)
            self.receive(keywords=dtls_join_cmd.expected_responses,
                         timeout=TIMEOUT_DTLS_JOIN_COMMAND, must_quit=True)
        else:
            self.send(join_cmd.request, dev_class)
            self.receive(keywords=join_cmd.expected_responses,
                         timeout=TIMEOUT_JOIN_COMMAND, must_quit=True)

    def set_traces(self, enable):
        self.send(set_traces_cmd.request, enable)
        self.receive(keywords=set_traces_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def set_dr(self, dr):
        '''
        Sets data rate

        Parameters:
            dr (int): Data rate
        '''
        self.send(set_dr_cmd.request, dr)
        self.receive(keywords=set_dr_cmd.expected_responses,
                     timeout=TIMEOUT_JOIN_COMMAND, must_quit=True)

    def set_template(self, template_path):
        '''
        Sets a communication template

        Parameters:
            template_path (pathlib.Path): File path to the template
        '''
        template = bytearray()
        with open(template_path, "rb") as f:
            byte = f.read(1)
            while byte:
                template += byte
                byte = f.read(1)
        self.send(set_template_cmd.request, template.hex())
        self.receive(keywords=set_template_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)
    
    def set_polling_conf(self, enable_polling):
        '''
        Makes the ATModem suspend uplinks and send an empty frame in order to trigger downlinks in class A during fragmentation session
       
        Parameters:
            enable polling during fragmentation session (bool)
        '''
        self.send(set_polling_conf_cmd.request, enable_polling)
        self.receive(keywords=set_polling_conf_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def trigger_polling(self):
        '''
        Makes the ATModem send an empty frame in order to trigger downlinks in class A
        '''
        self.send(trigger_polling_cmd.request)
        self.receive(keywords=trigger_polling_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def sync(self, retransmission_tmr_dur, retransmission_cnt):
        '''
        Retrieves communication parameters from SCHC gateway

        Parameters:
            retransmission_tmr_dur (int): Duration between consequent sync retransmission requests
            retransmission_cnt (int): Maximum number of sync retransmission requests
        '''
        self.send(sync_cmd.request, retransmission_tmr_dur,
                  retransmission_cnt)
        self.receive(keywords=sync_cmd.expected_responses,
                     timeout=retransmission_tmr_dur * retransmission_cnt, must_quit=True)

    def set_communication_param(self, index, value):
        '''
        Sets communication parameter associated to the index provided

        Parameters:
            index (string): communication parameter index
            value (string): communication parameter value
        '''
        self.send(set_template_param_cmd.request, index, value)
        self.receive(keywords=set_template_param_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def get_communication_param(self, index):
        '''
        Gets communication parameter associated to the index provided

        Return:
            param (string): Requested communication parameter
        '''
        self.send(get_template_param_cmd.request, index)
        response = self.receive(
            timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)
        # error received (either param is not found or not configured)
        if get_template_param_cmd.expected_responses[1] in response:
            return None
        print(response)
        # response can be parsed to its index and value
        index, param = response.split(',')
        # to get sucess response
        self.receive(keywords=get_template_param_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND)
        return param

    def sendb_data(self, packet):
        '''
        Sends data using SDK with NET API

        Parameters:
            packet (bytes): Data to be sent, encoded as hex
        '''
        self.send(sendb_data_cmd.request, packet.hex())
        response = self.receive(keywords=sendb_data_cmd.expected_responses)
        status = 0
        error = 0
        if (response is not None and response.find("SENDFAIL") != -1):
            status, error = [int(s)
                             for s in response.split(',') if s.isdigit()]
        return (status, error)

    def receive_data(self, time_to_wait):
        '''
        Returns received data

        Parameters:
            time_to_wait (int): Maximum time to wait to receive data

        Returns:
            string: Data received
        '''
        status = 0
        msg = self.receive(
            keywords=receive_data_cmd.expected_responses, timeout=time_to_wait)
        if msg is not None and len(msg) > 0:
            if msg.startswith("+RECVOK,"):
                msg = bytearray.fromhex(msg.split(',')[1])
            elif msg.startswith("+RECVFAIL,"):
                msg = None
                status = msg.split(',')[1]
        return (status, msg)

    def conf_dtls(self, psk_id, psk, dtls_timeout, dtls_retrans_cnt):
        '''
        Configures the DTLS layer

        Parameters:
            psk_id: PSK identifier
            psk: PSK value
            dtls_timeout: the amount of time in ms before a handshake is
                          considered timed out
            dtls_retrans_cnt: the maximum amount of retransmissions to attempt
                              during the DTLS handshake
        '''
        self.dtls = True
        self.send(conf_dtls_cmd.request, psk_id, psk,
                  dtls_retrans_cnt, dtls_timeout)
        self.receive(keywords=conf_dtls_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND)

    def disable_echo(self):
        '''
        Disables the echo from the board
        '''
        self.send(disable_echo_cmd.request, 0)
        self.receive(keywords=disable_echo_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND)

    def set_modem_options(self, parser):
        parser.add_argument("--template", type=pathlib.Path, required=True,
                            help="set template file")
        parser.add_argument("--dev-eui", type=str, required=True,
                            help="set device EUI in semi colon separated 16 bytes format")
        parser.add_argument("--app-eui", type=str, required=True,
                            help="set application EUI in semi colon separated 16 bytes format")
        parser.add_argument("--app-key", type=str, required=True,
                            help="set application key in semi colon separated 32 bytes format")
        parser.add_argument("--ipv6-dev-prefix", type=str, required=True,
                            help="set the device IPv6 prefix (represented in hex separated by colons)")
        parser.add_argument("--ipv6-dev-iid", type=str,
                            help="set the device IPv6 IID")
        parser.add_argument("--udp-dev-port", type=int,
                            help="set the device UDP port")
        parser.add_argument("--ipv6-app-prefix", type=str,
                            help="set the application IPv6 prefix (represented in hex separated by colons)")
        parser.add_argument("--ipv6-app-iid", type=str,
                            help="set the application IPv6 IID")
        parser.add_argument("--udp-app-port", type=int,
                            help="set the application UDP port")
        parser.add_argument("--dev-class", type=str, default='C',
                            choices="A, B, C", help="set device class")
        parser.add_argument("--dr", type=int, default=5,
                            help="set data rate")
        parser.add_argument("--sync", action='store_true',
                            default=False, help="enable synchronization")
        parser.add_argument("--sync-retrans-dur", type=int, default=10000,
                            help="set sync retransmission duration in ms")
        parser.add_argument("--sync-retrans-cnt", type=int, default=3,
                            help="set sync retransmission count")
        parser.add_argument("--traces", action='store_true', default=False)
        parser.add_argument("--psk-id", help="DTLS PSK ID", required=False, default=None)
        parser.add_argument("--psk", help="DTLS PSK value",
                            required=False, default=None)
        parser.add_argument("--dtls-timeout", type=int, default=60000,
                            help="set DTLS handshake timeout in ms")
        parser.add_argument("--dtls-retrans-cnt", type=int, default=3,
                            help="set sync retransmission count")
        parser.add_argument("--polling-interval", type=int, default=60,
                        help='Used in conjunction with class A in order to determine at which interval polling messages should be sent, duration is in sec and minimal value is 60 sec')
        parser.add_argument("--polling-conf", type=int, default=0,
                        help='Used in conjunction with class A in order to send polling messages during downlink fragmentation session. 0 to disable this feature, 1 to enable it')

    def check_timers(self):
        if self._dev_class == 'A' and self._polling_interval != 0:
            if time.time() > self.timeout:
                self.trigger_polling()
                self.timeout = time.time() + self._polling_interval

    def output_ready(self):
        return self.read_ready()

    @property
    def modem_name(self):
        return self._modem_name
