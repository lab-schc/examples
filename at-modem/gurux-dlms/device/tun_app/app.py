# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

import argparse
import logging

from .atmodem import ATModem
from .serial_device import Serial_Device
from .app_config import App_Config
from .tun import Tunnel


def run():
    app_config = App_Config()
    parser = argparse.ArgumentParser(
        description="Lab.SCHC Device Application")

    device = Serial_Device()
    modem = ATModem(device)

    app_config.set_app_param_options(parser)
    modem.set_modem_options(parser)

    args = parser.parse_args()

    app_config.set_app_options(args)
    modem.configure_modem(args)

    # Setup logging
    log_level = {'error': logging.ERROR, 'warning': logging.WARNING,
                 'info': logging.INFO, 'debug': logging.DEBUG}
    logging.basicConfig(
        format="%(asctime)s %(levelname)-8s %(message)s",
        level=log_level[args.log_level],
        filename=args.logfile
    )

    tun = Tunnel(modem, app_config.tun_name)
    logging.info("Device configured")
    try:
        tun.tun_loop()
    except KeyboardInterrupt:
        tun.close()
