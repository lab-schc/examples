# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

import serial
import serial.tools.list_ports
import logging
import time

from threading import Lock
from collections import namedtuple

SLEEP_TIME_AFTER_WRITE = 0.5
SERIAL_READ_TIMEOUT = 0.3
EMPTY_LINE = '\r\n'

Module = namedtuple('Module', 'name vid pid baudrate device')
modem = Module('ATModem', 0x0483, 0x374B, 9600, '/dev/ttyACM0')


class Serial_Device:
    def __init__(self):
        self.lock = Lock()
        self.serial = serial.Serial(timeout=SERIAL_READ_TIMEOUT)
        self.scan_module()
        self.open()

    def open(self):
        '''Opens serial port'''
        try:
            self.serial.open()
            self.reset()
        except Exception as e:
            logging.error('Error opening serial port')
            raise e

    def close(self):
        '''Closes serial port'''
        try:
            self.reset()
            self.serial.close()
        except Exception as e:
            logging.error('Error closing serial port')
            raise e

    def read_ready(self):
        if self.serial.in_waiting > 0:
            return True
        return False

    def read(self):
        '''
        Reads data from the serial port

        Returns:
            data (bytearray): Data read
        '''
        data = b''
        while True:
            with self.lock:
                byte = self.serial.read()
            if len(byte) == 0:
                break
            data = data + byte
        return data

    def readline(self):
        '''
        Reads line from the serial port

        Returns:
            data (bytearray): Data read (None if no byte is read)
        '''
        while True:
            with self.lock:
                byte = self.serial.readline()
            # give up when expected number of bytes are not read
            if len(byte) == 0:
                return None
            return byte.decode('utf-8').strip(EMPTY_LINE)

    def write(self, data):
        '''
        Writes to the serial port

        Parameters:
            data (bytes): Data to be written
        '''
        # it's important to reset before and after sending any command
        # otherwise we might pollute the fd and subsequent reads could return garbage
        with self.lock:
            self.reset()
            self.serial.write(data)
            self.reset()
        # we give time to module to send its response
        time.sleep(SLEEP_TIME_AFTER_WRITE)

    def reset(self):
        '''Flushes all buffers'''
        self.serial.flush()
        self.serial.reset_input_buffer()
        self.serial.reset_output_buffer()

    def scan_module(self):
        '''Scans and auto-configures serial port'''
        for p in serial.tools.list_ports.comports(include_links=False):
            if p.vid == modem.vid and p.pid == modem.pid and modem.device == p.device:
                self.serial.port = p.device
                self.serial.baudrate = modem.baudrate
