# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

import sys
import logging
import datetime
import traceback


class Module:
    def __init__(self, serial, line_terminator='\n'):
        self.line_terminator = line_terminator
        self.serial = serial

    def send(self, cmd, *args):
        '''
        Sends AT command request

        Parameters:
            cmd (string): Request
            args (string): Request argements
        '''
        cmd_str = cmd.format(*args)
        logging.debug('request  : {}'.format(cmd_str))
        self.serial.write(str.encode(cmd_str + self.line_terminator))

    def send_binary(self, data):
        logging.debug('data sent  : {}'.format(data))
        self.serial.write(data)

    def receive(self, keywords=None, timeout=0, must_quit=False, binary=False):
        '''
        Reads serial device response until the expected keyword is found or the
        timeout expires

        Parameters:
            keywords (string): String list to be compared within the response
            timeout (int): Value of the timeout to be used in seconds
            must_quit (bool): Flag that indicates if the application must quit if
            the timer expires

        Returns:
            string: Response
        '''
        start = datetime.datetime.now()
        while True:
            # Timeout handling
            if timeout != 0 and datetime.datetime.now() > start + datetime.timedelta(seconds=timeout):
                if must_quit is True:
                    logging.error(
                        'Timeout on critical response receive, quitting the application...')
                    self.serial.close()
                    sys.exit(1)
                else:
                    return None
            # Serial read handling
            response = self.serial.readline() if binary is False else self.serial.read()
            if response != None and len(response) > 0:
                logging.debug('response : {}'.format(str(response)))
                if keywords is not None:
                    for keyword in keywords:
                        if str(response).find(keyword) != -1:
                            return response
                    continue
                else:
                    return response
            else:
                # Note: if timeout is not specified and there is no response, it
                # is possible to be stuck here
                continue

    def read_ready(self):
        return self.serial.read_ready()
