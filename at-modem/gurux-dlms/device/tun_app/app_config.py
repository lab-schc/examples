# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

class App_Config:
    @property
    def tun_name(self):
        return self._tun_name

    def set_app_param_options(self, parser):
        parser.add_argument("--tun-name", type=str, default="dlms_tun",
                            help="set the name of the TUN interface to use")
        parser.add_argument("--log-level", default="info", choices="error, warning, info, debug",
                            help="Application's log level")
        parser.add_argument("--logfile", type=str, default="/var/log/tuncontroller.log",
                            dest="logfile", help="File where the the app's logs will be written")

    def set_app_options(self, args):
        self._tun_name = args.tun_name
