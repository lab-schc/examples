# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

import scapy.all as scapy
import subprocess
import logging
import time
import os
import fcntl
import struct
import select
from threading import Lock, Thread

from .atmodem import ATModem
from .serial_device import Serial_Device

IFF_TUN = 0x0001
IFF_NO_PI = 0x1000
TUNSETIFF = 0x400454CA
IPV6_MTU = 1500
UDP_NXT_HDR = 17

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s', level='DEBUG')


class Tunnel:
    def __init__(self, modem, name, tun_file="/dev/net/tun"):
        self.queue = []
        self.uplink_lock = Lock()
        self.downlink_lock = Lock()
        self.tun_lock = Lock()
        self.modem_lock = Lock()
        self.uplink_queue = []
        self.downlink_queue = []
        self.running = True
        self.name = name.encode()
        self.modem = modem

        self.tun_fd = os.open(tun_file, os.O_RDWR)
        if self.tun_fd < 0:
            raise OSError

        flags = IFF_TUN | IFF_NO_PI
        ifreq = struct.pack('16sh', self.name, flags)
        fcntl.ioctl(self.tun_fd, TUNSETIFF, ifreq)

    def start_tun_listener(self):
        self.uplink_routine = Thread(target=self.tun_reading_routine)
        self.uplink_routine.start()

    def start_modem_listener(self):
        self.downlink_routine = Thread(target=self.modem_reading_routine)
        self.downlink_routine.start()

    def tun_reading_routine(self):
        pollster = select.poll()
        pollster.register(self.tun_fd, select.POLLIN)
        while self.running:
            time.sleep(.1)
            fdlist = pollster.poll(-1)
            if fdlist == []:
                continue
            fd, flags = fdlist[0]
            if fd != self.tun_fd or flags & select.POLLHUP:
                sys.exit(-1)
            elif flags & select.POLLIN:
                with self.tun_lock:
                    packet = os.read(self.tun_fd, IPV6_MTU)
                if packet is not None:
                    if (len(packet) > 28 and packet[9] == UDP_NXT_HDR) or (len(packet) > 48 and packet[6] == UDP_NXT_HDR):
                        with self.uplink_lock:
                            self.uplink_queue.append(packet)

    def tun_read(self):
        retval = None
        with self.uplink_lock:
            if len(self.uplink_queue) > 0:
                retval = self.uplink_queue.pop(0)
        return retval

    def modem_reading_routine(self):
        while self.running:
            time.sleep(.2)
            with self.modem_lock:
                if self.modem.output_ready() is True:
                    (status, msg) = self.modem.receive_data(5)
                else:
                    continue
            if status == 0 and msg is not None:
                logging.info("Reception successful")
                with self.downlink_lock:
                    self.downlink_queue.append(msg)
            elif status != 0:
                logging.error("Reception failure: %d" % status)

    def modem_read(self):
        retval = None
        with self.downlink_lock:
            if len(self.downlink_queue) > 0:
                retval = self.downlink_queue.pop(0)
        return retval

    def tun_loop(self):
        self.start_tun_listener()
        logging.info("Now reading from TUN")
        self.start_modem_listener()
        while 1:
            time.sleep(.1)
            uplink = self.tun_read()
            if uplink is not None:
                logging.info("Sending: {}".format(uplink))
                with self.modem_lock:
                    (status, error) = self.modem.sendb_data(uplink)
                if status != 0:
                    logging.error(
                        "Modem send failed: status: {} error: {}", status, error)
                else:
                    logging.info("Modem send success")
            downlink = self.modem_read()
            if downlink is not None:
                self.tun_write(downlink)
            with self.modem_lock:
                self.modem.check_timers()

    def close(self):
        self.running = False
        os.close(self.tun_fd)

    def tun_write(self, buf):
        with self.tun_lock:
            try:
                os.write(self.tun_fd, buf)
            except Exception as e:
                pass
