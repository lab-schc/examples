#!/bin/sh

if [ "$#" -ne 3 ]; then
    echo "Bad number of argument"
    echo "Usage: $0 <TUN name> <dev IPv6> <app prefix>/64"
    exit 1
fi

ip tuntap add mode tun name $1
ip link set $1 up
ip -6 addr add $2 dev $1
ip -6 route add $3 dev $1