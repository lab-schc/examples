# UDP client

## Description

This example provides a device application and a cloud application. The device
application sends messages to the cloud application on a periodic basis. The
cloud application displays every received message and sends back the message to
the device application. the device application displays every received the
message.

The example demonstrates how to use the SCHC SDK using the Datagram API through
AT commands with a cloud application expecting IPv6 UDP frames.

### SCHC compression/fragmentation

In this example, IPv6 and UDP layers are fully compressed by the SCHC SDK, so
only the UDP payload is sent over the air. This represents 48 bytes (40 + 8
bytes) that are not sent per message.

In case fragmentation is needed then:

* ACKonError is used for the downlink packets with rule ID 21
* ACKonError mode is used for the uplink packets,

The fragmentation rule is provisioned using the `--frag-profile` argument:
  * `--frag-profile device/rule_fragmentation_uplink_20.bin`: ACKonError mode
    is used with rule ID 20

### DTLS encryption

This application can use a SCHC library that provides security to the
transferred data by making use of the DTLS protocol, compressed SCHC packets are
ciphered using this protocol before being sent over the air.

If the appropriate firmware is loaded onto the board then DTLS encryption shall
be used by the SDK, in order to properly use this feature then the `--psk-id`
and `--psk` parameters must be given with the same values provided to the IPCore

## Class-A specification

To run this example using a class-A device, `--dev-class A` parameter must be
set. Besides, in order to enable polling message during a fragmentation session,
the parameter `--polling-conf 1` must be given. Polling messages interval may be
set with `--polling-interval X `, where `X` is the periodicity of the polling
message (default value is 60 seconds if not set).


## Requirements

* Ubuntu 20.04 LTS
* Python3

## Supported targets

* [Semtech
  LR1110DVK1TBKS](https://www.semtech.com/products/wireless-rf/lora-edge/lr1110dvk1tbks)
  development kit
* [STM32L476RG-Nucleo](https://os.mbed.com/platforms/ST-Nucleo-L476RG) board
  with
  [sx1261](https://www.semtech.com/products/wireless-rf/lora-core/sx1261mb2bas)
  extension shield
* [Nucleo-WL55JC](https://www.st.com/en/evaluation-tools/nucleo-wl55jc.html)

## Run the example

Please read the [online documentation](https://lab-schc.gitlab.io/docs/4-Manual/d1-apps/1-atm-udp/).
