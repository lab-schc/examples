# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

class App_Config:
    def __init__(self, args):
        self._bind_addr = (args.ip, args.port)
        self._delay = args.delay
        self._demo = args.demo

    @property
    def bind_addr(self):
        return self._bind_addr

    @property
    def delay(self):
        return self._delay

    @property
    def demo(self):
        return self._demo
