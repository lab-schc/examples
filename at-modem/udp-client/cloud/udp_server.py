# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

import socket
import time
import logging

from threading import Thread

DOWNLINK_MULTIPLIER = 1

recv_buff_size = 4096

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s', level='DEBUG')


class UDPServer():
    def __init__(self, app_config):
        self.bind_addr = app_config.bind_addr
        self.sock = None
        self.delay = app_config.delay
        self.demo = app_config.demo

    def configure_server(self):
        self.sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        self.sock.bind(self.bind_addr)
        logging.info('starting udp server on [{0}]:{1}'.format(
            self.bind_addr[0], self.bind_addr[1]))

    def wait_for_client(self):
        while True:
            try:
                payload, client_addr = self.sock.recvfrom(recv_buff_size)
                handler = Thread(target=self.handle_uplink,
                                 args=(client_addr, payload))
                handler.daemon = True
                handler.start()
            except OSError:
                logging.error('exception recvfrom...')

    def handle_uplink(self, client_addr, payload):
        logging.info('receive uplink packet from the device [{0}]:{1} {2}'.format(
                     client_addr[0], client_addr[1], payload))
        self.send_downlink(client_addr, payload)

    def send_downlink(self, client_addr, payload):
        time.sleep(self.delay)  # wait a bit after having received an uplink
        if (self.demo):
            # exchange Demo messages
            if b'Hello' in payload:
                dl_payload = b'Hello back! Confirmed: IPv6/UDP stack enabled.'
            else:
                dl_payload = b'Roger that! SCHC in action.'
            # dl_payload = bytes(answer, 'utf-8')
        else:
            # echo back payload
            dl_payload = payload * DOWNLINK_MULTIPLIER
        self.sock.sendto(dl_payload , client_addr)
        logging.info('send downlink packet to the device [{0}]:{1} {2}'.format(
            client_addr[0], client_addr[1], dl_payload))

    def shutdown_server(self):
        logging.info('shutting down server...')
        self.sock.close()

