# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

import argparse

from .udp_server import UDPServer
from .app_config import App_Config


def run():
    parser = argparse.ArgumentParser(description='Lab.SCHC Echo Server')
    parser.add_argument('-ip', type=str, required=True,
                        help='set application IPv6 address')
    parser.add_argument('-port', type=int, required=True,
                        help='set application UDP port')
    parser.add_argument('-delay', type=int, default=5,
                        help='set downlink delay in seconds following to uplink')
    parser.add_argument('-demo', type=bool, default=False,
                        help='expect Demo messages and answer back')
    args = parser.parse_args()

    app_config = App_Config(args)

    udp_server = UDPServer(app_config)
    udp_server.configure_server()

    try:
        udp_server.wait_for_client()
    except KeyboardInterrupt:
        udp_server.shutdown_server()
