# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

class App_Config:
    def __init__(self, args):
        self._delay = args.delay
        self._payload_len = args.payload_len

    @property
    def delay(self):
        return self._delay

    @property
    def payload_len(self):
        return self._payload_len
