# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

import argparse
import pathlib

from .udp_client import UDPClient
from .app_config import App_Config
from .modem_config import Modem_Config


def run():
    parser = argparse.ArgumentParser(
        description="Lab.SCHC Device Application")
    parser.add_argument("--template", type=pathlib.Path, required=True,
                        help="set template file")
    parser.add_argument("--frag-profile", type=pathlib.Path, required=False,
                        help="set fragmentation profile filepath", nargs="+")
    parser.add_argument("--dev-eui", type=str, required=True,
                        help="set device EUI in semi colon separated 16 bytes format")
    parser.add_argument("--app-eui", type=str, required=True,
                        help="set application EUI in semi colon separated 16 bytes format")
    parser.add_argument("--app-key", type=str, required=True,
                        help="set application key in semi colon separated 32 bytes format")
    parser.add_argument("--ipv6-dev-prefix", type=str, required=True,
                        help="set the device IPv6 prefix (represented in hex separated by colons)")
    parser.add_argument("--ipv6-dev-iid", type=str, required=False,
                        help="set the device IPv6 IID (represented in hex separated by colons)")
    parser.add_argument("--ipv6-app-prefix", type=str, required=False,
                        help="set the app IPv6 prefix (represented in hex separated by colons)")
    parser.add_argument("--ipv6-app-iid", type=str, required=False,
                        help="set the app IPv6 IID (represented in hex separated by colons)")
    parser.add_argument("--ipv6-dev-port", type=str, required=False,
                        help="set the device UDP port")
    parser.add_argument("--ipv6-app-port", type=str, required=False,
                        help="set the app UDP port")
    parser.add_argument("--delay", type=int, default=600,
                        help="set delay between two uplinks")
    parser.add_argument("--payload-len", type=int, default=64,
                        help="set payload length")
    parser.add_argument("--dev-class", type=str, default='C',
                        help="set device class")
    parser.add_argument("--dr", type=int, default=5,
                        help="set data rate")
    parser.add_argument("--sync-retrans-dur", type=int, default=10000,
                        help="set sync retransmission duration in ms")
    parser.add_argument("--sync-retrans-cnt", type=int, default=3,
                        help="set sync retransmission count")
    parser.add_argument("--psk-id", help="DTLS PSK ID",
                        required=False, default=None)
    parser.add_argument("--psk", help="DTLS PSK value",
                        required=False, default=None)
    parser.add_argument("--dtls-timeout", type=int, default=60000,
                        help="set DTLS handshake timeout in ms")
    parser.add_argument("--dtls-retrans-cnt", type=int, default=3,
                        help="set sync retransmission count")
    parser.add_argument("--polling-interval", type=int, default=60,
                        help='Used in conjunction with class A in order to determine at which interval polling messages should be sent, duration is in sec and minimal value is 60 sec')
    parser.add_argument("--polling-conf", type=int, default=0,
                        help='Used in conjunction with class A in order to send polling messages during downlink fragmentation session. 0 to disable this feature, 1 to enable it')
    args = parser.parse_args()

    app_config = App_Config(args)
    modem_config = Modem_Config(args)

    udp_client = UDPClient(app_config)
    udp_client.configure_device(modem_config)

    try:
        udp_client.modem_routine(modem_config)
    except KeyboardInterrupt:
        udp_client.shutdown_client()
