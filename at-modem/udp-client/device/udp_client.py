# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

import string
import time
import logging
import random
import threading

from threading import Lock, Thread
from .atmodem import ATModem
from .serial_device import Serial_Device


logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s', level='DEBUG')


class Communication_Parameters:
    def __init__(self, dev_prefix, dev_iid, app_prefix, app_iid, dev_port, app_port):
        self.dev_ip = self._convert_ip_format(dev_prefix, dev_iid)
        self.app_ip = self._convert_ip_format(app_prefix, app_iid)
        self.dev_port = str(int(dev_port, 16))
        self.app_port = str(int(app_port, 16))

    def _convert_ip_format(self, prefix, iid):
        ip = prefix + iid
        return ':'.join(ip[i:i+4] for i in range(0, len(ip), 4))

    def get_dev_ip_addr(self):
        return self.dev_ip

    def get_app_ip_addr(self):
        return self.app_ip

    def get_dev_udp_port(self):
        return self.dev_port

    def get_app_udp_port(self):
        return self.app_port


class UDPClient():
    def __init__(self, app_config):
        self.app_config = app_config
        device = Serial_Device()
        self.modem = ATModem(device)
        self.dev_ip = None
        self.dev_app = None
        self.dev_udp_port = None
        self.app_udp_port = None
        self.socket_id = None
        self.uplink_timeout = 0
        self.polling_timeout = 0
        self.thread_running = True
        self.thread = None
        self.modem_lock = Lock()

    def configure_device(self, modem_config):
        # self.modem.reset()
        self.modem.ping()
        self.modem.unset_echo()
        self.modem.set_dev_eui(modem_config.dev_eui)
        self.modem.set_app_eui(modem_config.app_eui)
        self.modem.set_app_key(modem_config.app_key)
        if modem_config.psk_id is not None and modem_config.psk is not None:
            self.modem.conf_dtls(modem_config.psk_id, modem_config.psk,
                                 modem_config.dtls_timeout, modem_config.dtls_retrans_cnt)
        self.modem.set_device_class(modem_config.dev_class) 
        self.modem.get_device_class()
        self.modem.join(modem_config.dev_class)
        self.modem.get_device_class()
        self.modem.set_dr(modem_config.dr)
        if modem_config.frag_profile:
            self.modem.set_frag_profile(modem_config.frag_profile)
            logging.info("configuring fragmentation profile")
        self.modem.set_template(modem_config.template)
        logging.info("configuring IPv6 device prefix: %s",
                     modem_config.ipv6_dev_prefix)
        self._set_ipv6_dev_prefix(modem_config.ipv6_dev_prefix)

        # Enable polling during downlink fragmentation session for class-A device
        if (modem_config.dev_class == 'A'):
            if (modem_config.polling_conf):
                # Enable polling and suspend uplinks during fragmentation session
                self.modem.set_polling_conf(1)
            else:
                # default
                self.modem.set_polling_conf(0)

        # Prepare communication/template parameters
        if (modem_config.ipv6_dev_iid == None):
            # Sync process over the air
            self.modem.sync(modem_config.sync_retrans_dur,
                            modem_config.sync_retrans_cnt)
        else:
            # Set template params manually
            # ipv6_dev_prefix already set.
            self._set_ipv6_dev_iid(modem_config.ipv6_dev_iid)
            self._set_ipv6_app_prefix(modem_config.ipv6_app_prefix)
            self._set_ipv6_app_iid(modem_config.ipv6_app_iid)
            self._set_ipv6_dev_port(modem_config.ipv6_dev_port)
            self._set_ipv6_app_port(modem_config.ipv6_app_port)
        self._prepare_communication_parameters()

        # Prepare DTG socket
        logging.info("Preparing DTG Socket...")
        self.modem.select_api('D')

        logging.info("device IP address: %s",  self.dev_ip)
        self._create_client()

    def _set_ipv6_dev_prefix(self, ipv6_dev_prefix):
        address = ipv6_dev_prefix.replace(':', '')
        self.modem.set_communication_param(0, address)

    def _set_ipv6_dev_iid(self, ipv6_dev_iid):
        address = ipv6_dev_iid.replace(':', '')
        self.modem.set_communication_param(1, address)

    def _set_ipv6_app_prefix(self, ipv6_app_prefix):
        address = ipv6_app_prefix.replace(':', '')
        self.modem.set_communication_param(2, address)

    def _set_ipv6_app_iid(self, ipv6_app_iid):
        address = ipv6_app_iid.replace(':', '')
        self.modem.set_communication_param(3, address)

    def _set_ipv6_dev_port(self, ipv6_dev_port):
        port = f'{int(ipv6_dev_port):04x}'
        self.modem.set_communication_param(4, port)

    def _set_ipv6_app_port(self, ipv6_app_port):
        port = f'{int(ipv6_app_port):04x}'
        self.modem.set_communication_param(5, port)

    def _prepare_communication_parameters(self):
        dev_prefix = self.modem.get_communication_param(0)
        dev_iid = self.modem.get_communication_param(1)
        app_prefix = self.modem.get_communication_param(2)
        app_iid = self.modem.get_communication_param(3)
        dev_port = self.modem.get_communication_param(4)
        app_port = self.modem.get_communication_param(5)
        comm_param = Communication_Parameters(dev_prefix, dev_iid, app_prefix,
                                              app_iid, dev_port, app_port)
        self.dev_ip = comm_param.get_dev_ip_addr()
        self.app_ip = comm_param.get_app_ip_addr()
        self.dev_udp_port = comm_param.get_dev_udp_port()
        self.app_udp_port = comm_param.get_app_udp_port()

    def _create_client(self):
        self.socket_id = self.modem.create_socket()
        self.modem.bind_socket(self.socket_id, self.dev_ip, self.dev_udp_port)

    def modem_routine(self, modem_config):
        # Create a thread to manage downlink payload
        self.thread = Thread(target=self.modem_reading_routine)
        self.thread.start() 

        while True:
            time.sleep(.2)
            # Send an uplink periodically (every 60 sec)
            if time.time() > self.uplink_timeout:
                with self.modem_lock:
                    payload = ''.join(random.choice(string.ascii_uppercase)
                            for _ in range(self.app_config.payload_len))
                    self.modem.send_data(self.socket_id, self.app_ip,
                                        self.app_udp_port, payload)
                    self.uplink_timeout = time.time() + 60

            # Trigger a polling message every modem_config.polling_interval
            if modem_config.dev_class == 'A' and modem_config.polling_interval != 0 and time.time() > self.polling_timeout:
                with self.modem_lock:
                    self.modem.trigger_polling()
                    self.polling_timeout = time.time() + modem_config.polling_interval


    def modem_reading_routine(self):
        while self.thread_running:
            time.sleep(.2)
            with self.modem_lock:
                # 5 seconds timeout for payload reception
                self.modem.receive_data(5)

    def shutdown_client(self):
        logging.info('shutting down client...')
        self._serial.close()
        if self.thread is not None:
            self.thread_running = False
            self.thread.join()
