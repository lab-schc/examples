# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

import logging

from .module import Module
from collections import namedtuple

Command = namedtuple('Command', 'request expected_responses')
reset_cmd = Command('ATZ', ['OK'])
ping_cmd = Command('AT+SCHC=VERSION', ['OK'])
unset_echo_cmd = Command('ATE=0', ['OK'])
set_dev_eui_cmd = Command('AT+DEUI={}', ['OK'])
set_app_eui_cmd = Command('AT+APPEUI={}', ['OK'])
set_app_key_cmd = Command('AT+APPKEY={}', ['OK'])
join_cmd = Command('AT+JOIN={}', ['+JOINED'])
dtls_join_cmd = Command('AT+JOINDTLS={}', ['+HANDSHAKEOK'])
sync_cmd = Command('AT+SCHC=SYNC,{},{}', ['+SYNCOK'])
set_frag_cmd = Command('AT+SCHC=FRAG,SET,{}', ['OK'])
set_template_cmd = Command('AT+SCHC=TPL,SET,{}', ['OK'])
get_template_id_cmd = Command('AT+SCHC=TPL,GETID', ['OK'])
get_device_class_cmd = Command('AT+CLASS=?', ['A','B','C','OK'])
set_device_class_cmd = Command('AT+CLASS={}', ['A','B','C','OK'])
set_template_param_cmd = Command(
    'AT+SCHC=TPLPARAMS,SET,{},{}', ['OK', 'AT_ERROR,1,1'])
get_template_param_cmd = Command(
    'AT+SCHC=TPLPARAMS,GET,{0}', ['OK', 'AT_ERROR,1,1'])
select_api_cmd = Command('AT+SCHC=API,{}', ['OK'])
create_socket_cmd = Command('AT+SCHC=SOCKET', ['OK'])
bind_socket_cmd = Command('AT+SCHC=BIND,{},{},{}', ['OK'])
send_data_cmd = Command('AT+SCHC=SEND,{},{},{},{}', ['+SENDOK', '+SENDFAIL'])
set_dr_cmd = Command('AT+DR={}', ['OK'])
conf_dtls_cmd = Command('AT+SCHC=DTLSCONF,SET,{},{},{},{}', ['OK'])
receive_data_cmd = Command(None, ['+RECVOK', '+RECVFAIL'])
set_polling_conf_cmd = Command('AT+SCHC=CONF_POLL,{},1', ['OK'])
trigger_polling_cmd = Command('AT+SCHC=TRG_POLL', ['OK'])

TIMEOUT_DEFAULT_COMMAND = 1
TIMEOUT_SEND_COMMAND = 10
TIMEOUT_JOIN_COMMAND = 20
TIMEOUT_DTLS_JOIN_COMMAND = 30


class ATModem(Module):
    def __init__(self, device):
        super().__init__(device)
        self.dtls = False

    def ping(self):
        '''Checks whether module is responding'''
        self.send(ping_cmd.request)
        self.receive(keywords=ping_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def reset(self):
        '''Resets device'''
        self.send(reset_cmd.request)
        self.receive(keywords=reset_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=False)

    def unset_echo(self):
        ''' Disable command request echo '''
        self.send(unset_echo_cmd.request)
        self.receive(keywords=unset_echo_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND,
                     must_quit=True)

    def set_dev_eui(self, dev_eui):
        '''
        Sets device EUI

        Parameters:
            dev_eui (string): Device EUI
        '''
        self.send(set_dev_eui_cmd.request, dev_eui)
        self.receive(keywords=set_dev_eui_cmd.expected_responses,
                     timeout=TIMEOUT_JOIN_COMMAND, must_quit=True)

    def set_app_eui(self, app_eui):
        '''
        Sets application EUI

        Parameters:
            app_eui (string): Application EUI
        '''
        self.send(set_app_eui_cmd.request, app_eui)
        self.receive(keywords=set_app_eui_cmd.expected_responses,
                     timeout=TIMEOUT_JOIN_COMMAND, must_quit=True)

    def set_app_key(self, app_key):
        '''
        Sets application key

        Parameters:
            app_key (string): Application key
        '''
        self.send(set_app_key_cmd.request, app_key)
        self.receive(keywords=set_app_key_cmd.expected_responses,
                     timeout=TIMEOUT_JOIN_COMMAND, must_quit=True)

    def join(self, dev_class):
        '''
        Joins to the network server and configures device class

        Parameters:
            dev_class (string): Class
        '''
        if self.dtls is True:
            self.send(dtls_join_cmd.request, dev_class)
            self.receive(keywords=dtls_join_cmd.expected_responses,
                         timeout=TIMEOUT_DTLS_JOIN_COMMAND, must_quit=True)
        else:
            self.send(join_cmd.request, dev_class)
            self.receive(keywords=join_cmd.expected_responses,
                        timeout=TIMEOUT_JOIN_COMMAND, must_quit=True)

    def set_dr(self, dr):
        '''
        Sets data rate

        Parameters:
            dr (int): Data rate
        '''
        self.send(set_dr_cmd.request, dr)
        self.receive(keywords=set_dr_cmd.expected_responses,
                     timeout=TIMEOUT_JOIN_COMMAND, must_quit=True)

    def set_frag_profile(self, frag_profile_list):
        '''
        Sets a fragmentation profile

        Parameters:
            frag_profile_path (pathlib.Path): File path to the fragmentatino profile
        '''
        for frag_profile_path in frag_profile_list:
            template = bytearray()
            with open(frag_profile_path, "rb") as f:
                byte = f.read(1)
                while byte:
                    template += byte
                    byte = f.read(1)
            self.send(set_frag_cmd.request, template.hex())
            self.receive(keywords=set_frag_cmd.expected_responses,
                         timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def set_template(self, template_path):
        '''
        Sets a communication template

        Parameters:
            template_path (pathlib.Path): File path to the template
        '''
        template = bytearray()
        with open(template_path, "rb") as f:
            byte = f.read(1)
            while byte:
                template += byte
                byte = f.read(1)
        self.send(set_template_cmd.request, template.hex())
        self.receive(keywords=set_template_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def get_template_id(self):
        '''
        get template id
        '''
        self.send(get_template_id_cmd.request)
        self.receive(keywords=get_template_id_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def set_device_class(self, dev_class):
        '''
        Sets device class

        Parameters:
            dev_class (string): device class (A|B|C)
        '''
        self.send(set_device_class_cmd.request, dev_class)
        self.receive(keywords=set_device_class_cmd.expected_responses,
                     timeout=TIMEOUT_JOIN_COMMAND, must_quit=False)

    def get_device_class(self):
        '''
        get device class
        '''
        self.send(get_device_class_cmd.request)
        self.receive(keywords=get_device_class_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def set_communication_param(self, index, value):
        '''
        Sets communication parameter associated to the index provided

        Parameters:
            index (string): communication parameter index
            value (string): communication parameter value
        '''
        self.send(set_template_param_cmd.request, index, value)
        self.receive(keywords=set_template_param_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def sync(self, retransmission_tmr_dur, retransmission_cnt):
        '''
        Retrieves communication parameters from SCHC gateway

        Parameters:
            retransmission_tmr_dur (int): Duration between consequent sync retransmission requests
            retransmission_cnt (int): Maximum number of sync retransmission requests
        '''
        self.send(sync_cmd.request, retransmission_tmr_dur, retransmission_cnt)
        self.receive(keywords=sync_cmd.expected_responses,
                     timeout=retransmission_tmr_dur * retransmission_cnt, must_quit=True)

    def get_communication_param(self, index):
        '''
        Gets communication parameter associated to the index provided

        Return:
            index (string): Requested communication parameter index
        '''
        self.send(get_template_param_cmd.request, index)
        response = self.receive(
            timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)
        # error received (either param is not found or not configured)
        if get_template_param_cmd.expected_responses[1] in response:
            return None
        # response can be parsed to its index and value
        index, param = response.split(',')
        # to get sucess response
        self.receive(keywords=get_template_param_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND)
        return param

    def select_api(self, api):
        '''
        Selects the API to be used. Network or Datagram API

        Parameters:
            api (string): N or D
        '''
        self.send(select_api_cmd.request, api)
        self.receive(keywords=select_api_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def create_socket(self):
        '''
        Creates a datagram socket

        Return:
            socket_id (string) ID of the newly created socket
        '''
        self.send(create_socket_cmd.request)
        response = self.receive(
            timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)
        self.receive(keywords=create_socket_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)
        return response

    def bind_socket(self, socket_id, ip_addr, port):
        '''
        Binds the created socket to a destination IP and port

        Parameters:
            socket_id (string): Socket ID
            ip (string): IP address
            port (string): Port
        '''
        self.send(bind_socket_cmd.request, socket_id, ip_addr, port)
        self.receive(keywords=bind_socket_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def send_data(self, socket_id, ip, port, payload):
        '''
        Sends data to a specified IP and port

        Parameters:
            socket_id (string): Socket ID
            ip (string): IP address
            port (string): Port
            payload (bytes): Data to be sent
        '''
        self.send(send_data_cmd.request, socket_id, ip, port, payload)
        self.receive(keywords=send_data_cmd.expected_responses,
                     timeout=TIMEOUT_SEND_COMMAND)

    def receive_data(self, time_to_wait):
        '''
        Returns received data

        Parameters:
            time_to_wait (int): Maximum time to wait to receive data

        Returns:
            string: Data received
        '''
        return self.receive(keywords=receive_data_cmd.expected_responses, timeout=time_to_wait)

    def conf_dtls(self, psk_id, psk, dtls_timeout, dtls_retrans_cnt):
        '''
        Configures the DTLS layer

        Parameters:
            psk_id: PSK identifier
            psk: PSK value
            dtls_timeout: the amount of time in ms before a handshake is
                          considered timed out
            dtls_retrans_cnt: the maximum amount of retransmissions to attempt
                              during the DTLS handshake
        '''
        self.dtls = True
        self.send(conf_dtls_cmd.request, psk_id, psk, dtls_retrans_cnt, dtls_timeout)
        self.receive(keywords=conf_dtls_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND)

    def set_polling_conf(self, enable_polling):
        '''
        Makes the ATModem suspend uplinks and send an empty frame in order to trigger downlinks in class A during fragmentation session
       
        Parameters:
            enable polling during fragmentation session (bool)
        '''
        self.send(set_polling_conf_cmd.request, enable_polling)
        self.receive(keywords=set_polling_conf_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND, must_quit=True)

    def trigger_polling(self):
        '''
        Makes the ATModem send an empty frame in order to trigger downlinks in class A
        '''
        self.send(trigger_polling_cmd.request)
        self.receive(keywords=trigger_polling_cmd.expected_responses,
                     timeout=TIMEOUT_DEFAULT_COMMAND)
        
