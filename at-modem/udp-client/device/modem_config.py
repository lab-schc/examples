# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

class Modem_Config:
    def __init__(self, args):
        self._dev_class = args.dev_class
        self._dev_eui = args.dev_eui
        self._app_eui = args.app_eui
        self._app_key = args.app_key
        self._dr = args.dr
        self._sync_retrans_dur = args.sync_retrans_dur
        self._sync_retrans_cnt = args.sync_retrans_cnt
        self._template = args.template
        self._frag_profile = args.frag_profile
        self._ipv6_dev_prefix = args.ipv6_dev_prefix
        self._ipv6_dev_iid = args.ipv6_dev_iid
        self._ipv6_app_prefix = args.ipv6_app_prefix
        self._ipv6_app_iid = args.ipv6_app_iid
        self._ipv6_dev_port = args.ipv6_dev_port
        self._ipv6_app_port = args.ipv6_app_port
        self._psk_id = args.psk_id
        self._psk = args.psk
        self._dtls_timeout = args.dtls_timeout
        self._dtls_retrans_cnt = args.dtls_retrans_cnt
        self._polling_conf = args.polling_conf
        self._polling_interval = args.polling_interval

    @property
    def dev_class(self):
        return self._dev_class

    @property
    def dev_eui(self):
        return self._dev_eui

    @property
    def app_eui(self):
        return self._app_eui

    @property
    def app_key(self):
        return self._app_key

    @property
    def dr(self):
        return self._dr

    @property
    def sync_retrans_dur(self):
        return self._sync_retrans_dur

    @property
    def sync_retrans_cnt(self):
        return self._sync_retrans_cnt

    @property
    def template(self):
        return self._template

    @property
    def frag_profile(self):
        return self._frag_profile

    @property
    def ipv6_dev_prefix(self):
        return self._ipv6_dev_prefix

    @property
    def ipv6_dev_iid(self):
        return self._ipv6_dev_iid

    @property
    def ipv6_app_prefix(self):
        return self._ipv6_app_prefix

    @property
    def ipv6_app_iid(self):
        return self._ipv6_app_iid

    @property
    def ipv6_dev_port(self):
        return self._ipv6_dev_port

    @property
    def ipv6_app_port(self):
        return self._ipv6_app_port

    @property
    def psk_id(self):
        return self._psk_id

    @property
    def psk(self):
        return self._psk

    @property
    def dtls_timeout(self):
        return self._dtls_timeout

    @property
    def dtls_retrans_cnt(self):
        return self._dtls_retrans_cnt
    
    @property
    def polling_conf(self):
        return self._polling_conf

    @property
    def polling_interval(self):
        return self._polling_interval

