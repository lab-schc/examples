# Copyrigt 2024 ACTILITY SA - All Rights Reserved
# Copyright 2021 ACKLIO SAS 
# 
# This software is licensed under the MIT License.
# For details, see the LICENSE file or visit:
# https://opensource.org/licenses/MIT

import serial
import serial.tools.list_ports
import logging
import time

from collections import namedtuple

SERIAL_DEVICE = '/dev/ttyACM0'

SLEEP_TIME_AFTER_WRITE = 0.5
# Return after 100 ms if a char can't be read.
# Note: this timeout must be greater than the longest inter-character
# period that can be expected in an AT response (see comment in readline).
SERIAL_READ_TIMEOUT = 0.1
EMPTY_LINE = '\r\n'

Module = namedtuple('Module', 'name vid pid baudrate device')
modem = Module('ATModem', 0x0483, 0x374B, 9600, SERIAL_DEVICE)


class Serial_Device:
    def __init__(self):
        self.serial = serial.Serial(timeout=SERIAL_READ_TIMEOUT)
        self._scan_module()
        self.open()

    def open(self):
        '''Opens serial port'''
        try:
            self.serial.open()
            self._reset()
        except Exception as e:
            logging.error('error opening serial port')
            raise e

    def close(self):
        '''Closes serial port'''
        try:
            self._reset()
            self.serial.close()
        except Exception as e:
            logging.error('error closing serial port')
            raise e

    def readline(self):
        '''
        Reads line from the serial port

        Returns:
            data (bytearray): Data read (None if no byte is read)
        '''
        read_bytes = bytearray()
        while True:
            byte = self.serial.read(1)
            # Give up when expected number of bytes are not read.
            # Beware: if two characters are separated by a period of time 
            # longer than SERIAL_READ_TIMEOUT; then the start of the line
            # is ignored.
            if len(byte) == 0:
                return None
            read_bytes += bytearray(byte)
            # Return when end of line is received.
            if byte == b'\n':
                return read_bytes.decode('utf-8').strip(EMPTY_LINE)

    def write(self, data):
        '''
        Writes to the serial port

        Parameters:
            data (bytes): Data to be written
        '''
        # it's important to reset before sending any command otherwise we are
        # reading data that has been in the pipe for some time
        self._reset()
        self.serial.write(data)
        # we give time to module to send its response
        time.sleep(SLEEP_TIME_AFTER_WRITE)

    def _reset(self):
        '''Flushes all buffers'''
        self.serial.flush()
        self.serial.reset_input_buffer()
        self.serial.reset_output_buffer()

    def _scan_module(self):
        '''Scans and auto-configures serial port'''
        for p in serial.tools.list_ports.comports(include_links=False):
            if p.vid == modem.vid and modem.device == p.device:
                self.serial.port = p.device
                self.serial.baudrate = modem.baudrate
