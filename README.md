# Lab.SCHC Example Applications

Welcome to the Example Applications Repository for use with the [lab.SCHC
FullSDK](https://gitlab.com/lab-schc/sdk/full-sdk-delivery).
This repository contains a collection of example applications designed to
help you get started and explore the functionalities of our embedded SDK.

## Application examples

List of available applications:

### Using ATModem:

* [UDP Client](at-modem/udp-client/README.md)
* [Gurux DLMS](at-modem/gurux-dlms/README.md)

## Requirements

- [lab.SCHC FullSDK](https://gitlab.com/lab-schc/sdk/full-sdk-delivery)
- ST NUCLEO-L476RG + SX1276/SX1272 Semtech shield
- SCHC Gateway
- [Sample applications guide](https://lab-schc.gitlab.io/docs/4-Manual/d1-apps/0-apps/)

## Licenses

This repository is licensed under the [MIT License](https://opensource.org/licenses/MIT).
See the [LICENSE](LICENCE) file for more details.
